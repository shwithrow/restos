# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Ticket.delete_all
Menu.delete_all
CustomerReceipt.delete_all

Menu.create([item:"Hamburger Combo", cost:5.95])
Menu.create([item:"Pizza Combo", cost:6.95])
Menu.create([item:"Mexican Combo", cost:4.95])
Menu.create([item:"Italian Combo", cost:8.95])
Menu.create([item:"Kids Combo", cost:3.95])
