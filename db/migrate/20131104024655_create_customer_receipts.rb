class CreateCustomerReceipts < ActiveRecord::Migration
  def change
    create_table :customer_receipts do |t|
      t.integer :ticket_id
      t.float :cost
      t.float :tax
      t.float :total

      t.timestamps
    end
  end
end
