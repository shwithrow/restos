class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :item
      t.float :cost

      t.timestamps
    end
  end
end
