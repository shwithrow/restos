class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :table_number
      t.integer :menu_id
      t.string :item_status
      t.string :paid_status

      t.timestamps
    end
  end
end
