class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.integer :ticket_id
      t.integer :menu_id
      t.float :tax
      t.float :total

      t.timestamps
    end
  end
end
