require 'test_helper'

class CustomerReceiptsControllerTest < ActionController::TestCase
  setup do
    @customer_receipt = customer_receipts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customer_receipts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer_receipt" do
    assert_difference('CustomerReceipt.count') do
      post :create, customer_receipt: { cost: @customer_receipt.cost, tax: @customer_receipt.tax, ticket_id: @customer_receipt.ticket_id, total: @customer_receipt.total }
    end

    assert_redirected_to customer_receipt_path(assigns(:customer_receipt))
  end

  test "should show customer_receipt" do
    get :show, id: @customer_receipt
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer_receipt
    assert_response :success
  end

  test "should update customer_receipt" do
    put :update, id: @customer_receipt, customer_receipt: { cost: @customer_receipt.cost, tax: @customer_receipt.tax, ticket_id: @customer_receipt.ticket_id, total: @customer_receipt.total }
    assert_redirected_to customer_receipt_path(assigns(:customer_receipt))
  end

  test "should destroy customer_receipt" do
    assert_difference('CustomerReceipt.count', -1) do
      delete :destroy, id: @customer_receipt
    end

    assert_redirected_to customer_receipts_path
  end
end
