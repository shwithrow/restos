class CustomerReceiptsController < ApplicationController
  # GET /customer_receipts
  # GET /customer_receipts.json
  def index
    @customer_receipts = CustomerReceipt.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @customer_receipts }
    end
  end

  # GET /customer_receipts/1
  # GET /customer_receipts/1.json
  def show
    @customer_receipt = CustomerReceipt.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @customer_receipt }
    end
  end

  # GET /customer_receipts/new
  # GET /customer_receipts/new.json
  def new
    @customer_receipt = CustomerReceipt.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @customer_receipt }
    end
  end

  # GET /customer_receipts/1/edit
  def edit
    @customer_receipt = CustomerReceipt.find(params[:id])
  end

  # POST /customer_receipts
  # POST /customer_receipts.json
  def create
    @customer_receipt = CustomerReceipt.new(params[:customer_receipt])

    respond_to do |format|
      if @customer_receipt.save
        format.html { redirect_to @customer_receipt, notice: 'Customer receipt was successfully created.' }
        format.json { render json: @customer_receipt, status: :created, location: @customer_receipt }
      else
        format.html { render action: "new" }
        format.json { render json: @customer_receipt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /customer_receipts/1
  # PUT /customer_receipts/1.json
  def update
    @customer_receipt = CustomerReceipt.find(params[:id])

    respond_to do |format|
      if @customer_receipt.update_attributes(params[:customer_receipt])
        format.html { redirect_to @customer_receipt, notice: 'Customer receipt was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @customer_receipt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_receipts/1
  # DELETE /customer_receipts/1.json
  def destroy
    @customer_receipt = CustomerReceipt.find(params[:id])
    @customer_receipt.destroy

    respond_to do |format|
      format.html { redirect_to customer_receipts_url }
      format.json { head :no_content }
    end
  end
end
