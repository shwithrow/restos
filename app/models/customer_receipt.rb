class CustomerReceipt < ActiveRecord::Base
  attr_accessible :cost, :tax, :ticket_id, :total

  belongs_to :ticket

  def tax
    (self.read_attribute(:cost) * 0.0825).round(2)
  end

  def total
    self.total = self.cost + self.tax
  end
end
