class Receipt < ActiveRecord::Base
  attr_accessible :menu_id, :tax, :ticket_id, :total

  belongs_to :ticket #for ticket number
  belongs_to :menu #for cost

  def total
    self.total = self.cost * 0.0825
  end

  def cost
    self.cost = ticket.menu.cost
  end

  before_save do
    total
  end
end
