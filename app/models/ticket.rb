class Ticket < ActiveRecord::Base
  attr_accessible :item_status, :menu_id, :paid_status, :table_number

  belongs_to :menu #for item drop down
  has_many :customer_receipts #for ticket number

  validates_existence_of :menu
end
